<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

/**
 * 脚本指令
 *
 * 备注：1、php think hello
 *      2、php think hello 牧羊人
 *      3、php think hello 牧羊人 --city 南京市
 * @author 牧羊人
 * @since 2020-04-21
 */
class Hello extends Command
{

    /**
     * 任务配置
     */
    protected function configure()
    {
        // 任务描述
        $this->setName('hello')
            ->addArgument('name', Argument::OPTIONAL, "牧羊人")
            ->addOption('city', null, Option::VALUE_REQUIRED, '南京市')
            ->setDescription('Say Hello');
    }

    /**
     * 执行句柄
     *
     * @param Input $input
     * @param Output $output
     * @return int|null
     */
    protected function execute(Input $input, Output $output)
    {
        // 请求开始
        $output->writeln('请求开始：' . date('Y-m-d H:i:s'));

        // 处理过程
        $name = trim($input->getArgument('name'));
        $name = $name ?: 'thinkphp';
        if ($input->hasOption('city')) {
            $city = PHP_EOL . 'From ' . $input->getOption('city');
        } else {
            $city = '';
        }
        $output->writeln("Hello," . $name . '!' . $city);

        // 请求结束
        $output->writeln("请求结束:" . date('Y-m-d H:i:s'));
    }

}