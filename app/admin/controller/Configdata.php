<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\controller;


use app\admin\model\Config;
use app\admin\service\ConfigDataService;
use app\common\controller\Backend;
use think\facade\Db;
use think\facade\View;

/**
 * 配置管理-控制器
 * @author 牧羊人
 * @since 2020/7/1
 * Class Config
 * @package app\admin\controller
 */
class Configdata extends Backend
{
    /**
     * 初始化
     * @author 牧羊人
     * @since 2020/7/1
     */
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->model = new \app\admin\model\ConfigData();
        $this->service = new ConfigDataService();
    }

    /**
     * 数据列表页
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 牧羊人
     * @since 2020/7/1
     */
    public function index()
    {
        // 配置ID
        $config_id = request()->param('config_id', 1);

        // 获取配置
        $configModel = new Config();
        $configList = $configModel->where(['mark' => 1])->select();
        return parent::index([
            'config_id' => $config_id,
            'configList' => $configList->toArray(),
        ]); // TODO: Change the autogenerated stub
    }

    /**
     * 添加或编辑
     * @return mixed
     * @since 2020/7/1
     * @author 牧羊人
     */
    public function edit()
    {
        // 配置类型
        View::assign("typeList", config("admin.config_type"));
        // 配置
        View::assign("configList", Db::name('config')->where(['mark' => 1])->select()->toArray());
        // 配置ID
        $configId = request()->param('config_id', 0);
        // 初始化默认值
        return parent::edit([
            'config_id' => $configId,
        ]); // TODO: Change the autogenerated stub
    }

}