<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\admin\model\ActionLog;
use app\admin\service\LoginService;
use app\common\controller\Backend;
use think\View;

/**
 * 后台登陆控制器
 *
 * @author 牧羊人
 * @since 2020-04-22
 */
class Login extends Backend
{
    /**
     * 初始化方法
     * @author 牧羊人
     * @since 2020/4/22
     */
    public function initialize()
    {
        parent::initialize();
        $this->service = new LoginService();
    }

    /**
     * 登录首页
     * @return mixed
     * @author 牧羊人
     * @date 2019/4/18
     */
    public function index()
    {
        // 取消模板布局
        $this->app->view->layout(false);
        return \think\facade\View::fetch();
    }

    /**
     * 系统登录
     * @return mixed
     * @author 牧羊人
     * @date 2019/4/18
     */
    public function login()
    {
        if (IS_POST) {
            $result = $this->service->login();
            return $result;
        }
    }

    /**
     * 退出系统
     * @author 牧羊人
     * @since 2020/6/29
     */
    public function logout()
    {
        // 记录退出日志
        ActionLog::setTitle("系统退出");
        ActionLog::record();
        // 清空SESSION
        session('adminId', null);
        // 跳转登录页
        return redirect(url('/login/index'));
    }

}