<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_TP6_PRO混编版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2022 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 模板设置
// +----------------------------------------------------------------------

return [
    // 是否开启模板编译缓存,设为false则每次都会重新编译
    'tpl_cache'          => false,
    // 布局模板开关
    'layout_on'          => true,
    // 布局模板入口文件
    'layout_name'        => 'public/layout',
    // 布局模板的内容替换标识
    'layout_item'        => '{__CONTENT__}',
    // 视图输出字符串内容替换
    'tpl_replace_string' => [
        '__STATIC__'     => '/static',
        '__ADMIN__'      => '/static/admin',
        '__JS__'         => '/static/admin/js',
        '__CSS__'        => '/static/admin/css',
        '__IMAGES__'     => '/static/admin/images',
    ],
    'taglib_build_in'    => 'cx',
    // 预先加载的标签库
    'taglib_pre_load'    => implode(',', [
        \app\admin\widget\Widget::class,
        \app\admin\widget\Common::class,
        \app\admin\widget\Upload::class,
        \app\admin\widget\Itemcate::class,
        \app\admin\widget\Editor::class,
        \app\admin\widget\Layout::class,
        \app\admin\widget\Icon::class,
        \app\admin\widget\Checkbox::class,
        \app\admin\widget\City::class,
        \app\admin\widget\Date::class,
        \app\admin\widget\Transfer::class,
    ]),
];
